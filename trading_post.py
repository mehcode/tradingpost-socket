from flask import Flask, request
from flask_sockets import Sockets
from weakref import WeakKeyDictionary
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from gevent import monkey
import gevent
import json
import requests


monkey.patch_all()

app = Flask('trading_post')
sockets = Sockets(app)

# The key is the socket, and the value is the last version of the object that
# we were notified about.
listeners = WeakKeyDictionary()

# A history of the product catalog, so that we can check for changes.
# product_catalog = {}
product_catalog = set()

# Our stuff is namespaced in the TMF database so that we don't clobber other
# people at the hackathon.  This only applies to the product catalog api.
namespace_id = '664'

@sockets.route('/api/subscribe')
def subscribe(ws):
    """
    Notification to client:
    {
        "api": "ticket", # can be 'order' or 'catalog' also
        "type": "TicketChangedNotification"  # Directly from the source
        "data": {
            // Body of the notification.
            ...
        }
    }
    """

    while True:
        # Add the socket to the listeners
        listeners[ws] = {}

        print('New subscriber connected: %s' % ws)

        try:
            # A poor man's halt.  We really don't care about anything the
            # client sends us.
            message = ws.receive()
            if message is None:
                break
        except Exception:
            print('Socket exited abnormally')
            raise

def publish(data, api, event_type):

    # Broadcast the event to everyone who's subscribed
    # the iterable here is a set of (socket, sub_id) things
    for socket, history in listeners.items():
        # Remember, == does deep equality checking for dicts and iterables.
        if history.get(data.get('id')) == data:
            print('Droping duplicate message for socket %s' % (socket,))
            continue
        print('Notifying subscribed socket %s' % (socket,))
        client_data = {
            "api": api,
            "data": data,
            "type": event_type
        }

        # Add the item to the history
        history[data['id']] = data

        # Send the information to the socket.
        gevent.spawn(socket.send, json.dumps(client_data))


@app.route('/api/listener', methods=['POST'])
def recieve_message():
    """This is the callback endpoint for calls back to the platform.  Note
    that this really just sends us EVERYTHING regardless of what we post for
    every api.  So we need to do our own internal routing."""
    print(request.data)

    # jsonify the notification
    data = json.loads(request.data)

    # Check and see if we need ot actually publish information on this event
    # (does it contain our namespace?)
    if data['event'].get('type') != 'TradingPost5':
        print('Dropping non-namespaced request.')
        # Nope, not ours. Return a resolution code and move on with life.
        return '', 204

    if 'order' in data['eventType'].lower():
        api = 'order'
        print('Detected as Order')
    else:
        api = 'ticket'
        print('Detected as Ticket')

    # Publish everything!
    publish(data['event'], api, data['eventType'])

    # Just return a 204 No Content so that flask doesn't explode
    return '', 204

def update_catalog(once=False):
    # Constantly check and see if the catalog has new entries
    while True:
        url = \
            'http://env-4126955.jelastic.servint.net/DSProductCatalog/api/productOffering'
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            print('Some wierd server problem: %s' % response.status_code)
            continue

        for item in response.json():

            if item['id'] not in product_catalog:
                # Add it to the history!
                # product_catalog[item['id']] = item
                product_catalog.add(item['id'])

                # Assert that this new item is actually part of our namespace.
                for category in item['productCategories']:
                    if category.get('id') == namespace_id:
                        break
                else:
                    # nope, none of our stuff is here.
                    if not once:
                        print('Dropping non-namespaced message.')
                    continue
                if not once:
                    print('Located new product in product catalog: %s' % (item,))
                # We have a new thing!
                publish(item, 'catalog', "CatalogCreateNotification")

        if once:
            break

        # Sleep for 10 seconds so we're not constantly hammering the server
        gevent.sleep(10)


if __name__ == '__main__':
    # Poll the catalog api for changes since its not actually doing it itself.
    # Do one manually first so that we don't spam the console with garbage.
    update_catalog(True)
    thread = gevent.spawn(update_catalog)

    try:
        app.debug = True
        server = WSGIServer(('0.0.0.0', 80), app, handler_class=WebSocketHandler)
        server.serve_forever()
    except KeyboardInterrupt:
        # Make sure that we kil the thread before we continue
        thread.kill()
        raise
